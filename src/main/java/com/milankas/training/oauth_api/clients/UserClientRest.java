package com.milankas.training.oauth_api.clients;

import com.milankas.training.oauth_api.domain.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "microservice-user")
@RequestMapping("/v1/users")
public interface UserClientRest {

    @GetMapping("/search/findByUsername")
    UserDTO findByUsername(@RequestParam String username);

}
