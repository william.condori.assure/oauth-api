package com.milankas.training.oauth_api.domain.service;

import com.milankas.training.oauth_api.domain.dto.UserDTO;

public interface IUserService {

    UserDTO findByUsername(String username);

}
