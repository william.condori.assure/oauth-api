package com.milankas.training.oauth_api.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class RoleDTO {

    private UUID id;
    private String name;

}
