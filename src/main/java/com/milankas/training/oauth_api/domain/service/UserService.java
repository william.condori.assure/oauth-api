package com.milankas.training.oauth_api.domain.service;

import com.milankas.training.oauth_api.clients.UserClientRest;
import com.milankas.training.oauth_api.domain.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService, IUserService {

    @Autowired
    private UserClientRest userClientRest;

    @Override
    public UserDTO findByUsername(String username) {
        return userClientRest.findByUsername(username);
    }

    public List<GrantedAuthority> getAuthoritiesFromUser(UserDTO userDTO) {
        return userDTO.getRoles()
                .stream()
                .map(roleDTO -> new SimpleGrantedAuthority(roleDTO.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDTO userDTO = userClientRest.findByUsername(username);

        return new User(userDTO.getUsername(), userDTO.getPassword(), userDTO.getEnabled(),
                true, true, true, getAuthoritiesFromUser(userDTO));
    }
}
